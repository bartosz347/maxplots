## AAL-12-LS działka
Bartosz Wojciechowski  
  

####Kompilacja i uruchomienie
 * wymagania: CMake >= 3.9, g++/MinGW
 
Linux

    cd maxplots
    mkdir bin && cd bin
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make
    ./MaxPlots
      
Windows 

    cd MaxPlots
    mkdir bin && cd bin
    cmake -DCMAKE_BUILD_TYPE=Release -G "MinGW Makefiles" ..
    mingw32-make    
    .\MaxPlots.exe  
  
    

####Tryby uruchomienia
 
Uruchomienie algorytmu dla danych
  
    MaxPlots -i <data>
                  data format:
                   <width> <height> <pointsCount>
                   <x1> <y1>
                   <x2> <y2>
                   ...            

Wygenerowanie danych i uruchomienie algorytmu 

    MaxPlots -g <width> <height> <pointsCount>


Uruchomienie testów wydajnościowych

    MaxPlots -t <method: bf | dyn> <initial points count> <problems to solve> <step size> <tests per instance>
    
Uruchomienie testów jakościowych - porówanie wyników programowania dynamicznego z brute-force

    MaxPlots -q <initial points count> <problems to solve> <step size> <tests per instance>
    
    
####Wyniki
Program zwraca maksymalną ilość działek oraz listę punktów podziału
  
####Struktura
 * algorithms/ - algorytmy główne i pomocnicze
 * analysis/ - pomocnicze funkcje generujące analizę
 * structures/ - struktury danych
  
####Algorytm
 Program wykorzystuje algorytm brute-force z usprawnieniami ograniczającymi złożoność
 
####Przykładowe dane
plansza 6 x 5, 4 punkty

    6 5 4
    4 4
    5 3
    1 1
    3 2    