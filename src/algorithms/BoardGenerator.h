#ifndef MAXPLOTS_SCENEGENERATOR_H
#define MAXPLOTS_SCENEGENERATOR_H

#include <random>
#include <stdexcept>
#include <string>
#include <vector>
#include <time.h>
#include <chrono>

#include "../structures/Point.h"

class BoardGenerator {
public:
    BoardGenerator() //: mt(std::mt19937(rd()))
    {}

    std::vector<Point> generatePoints(int width, int height, int pointsCount)
    {
        std::random_device rdx;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        this->mt = std::mt19937(seed);

        if (pointsCount > std::min(height - 1, width - 1)) {
            throw std::runtime_error("too many points for field");
        }

        this->width = width;
        this->height = height;
        this->pointsLeft = pointsCount;
        this->points.clear();

/*        for (int i = 0; i < 30; ++i) {
            Point p = getRandomPoint();
            std::cout << p.x << std::endl;
        }*/

        while (pointsLeft > 0) {
            Point proposed = getRandomPoint();
            if (isUnique(proposed)) {
                points.push_back(proposed);
                pointsLeft--;
            }
        }

        return points;
    }

private:
    std::vector<Point> points;
    int pointsLeft;
    int width;
    int height;

    std::random_device rd;
    std::mt19937 mt;


    Point getRandomPoint()
    {
        std::uniform_int_distribution<int> xDist(1, width - 1);
        std::uniform_int_distribution<int> yDist(1, height - 1);

        return Point{xDist(mt), yDist(mt)};
    }

    bool isUnique(const Point &point) const
    {
        for (Point p : this->points) {
            if (p.x == point.x || p.y == point.y) {
                return false;
            }
        }
        return true;
    }


};


#endif //MAXPLOTS_SCENEGENERATOR_H
