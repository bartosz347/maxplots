#ifndef MAXPLOTS_RECURSIVEAPPROACH_H
#define MAXPLOTS_RECURSIVEAPPROACH_H

#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include "MaxPlotSolver.h"

class RecursiveApproach : public MaxPlotSolver {
public:
    static const int ALGORITHM_CODE = 3;

    int findMaxPlotsCount(int width, int height, const std::vector<Point> &pointsArg)
    {
        this->points = pointsArg;
        this->findPlotsResults.clear();

        std::sort(points.begin(), points.end(), [](Point a, Point b) {
            return a.x < b.x;
        });

        int plotsFound = findPlots(points, Borders{height, 0, 0, width});
        return plotsFound;
    }

    const std::vector<Point> &getDivisionPoints() const override
    {
        throw std::runtime_error("unsupported");
    }

private:
    std::map<std::string, int> findPlotsResults;
    std::vector<Point> points;

    int findPlots(std::vector<Point> &localPoints, Borders border)
    {
        // is field empty?
        if (localPoints.size() == 0) {
            return 0;
        }

        // is field a plot?
        if (localPoints.size() == 1) {
            return 1;
        }

        std::string id;
        id = generatePointslistIdentifier(localPoints);
        if (findPlotsResults.count(id) == 1) {
            return findPlotsResults.at(id);
        }

        int localMaxPlots = -1;

        // use each of localPoints as division point
        for (auto divisionPoint: localPoints) {
            std::vector<Point> topLeft;
            std::vector<Point> topRight;
            std::vector<Point> bottomRight;
            std::vector<Point> bottomLeft;

            // add each localPoint to one of four fields
            for (auto poi: localPoints) {
                if (poi == divisionPoint)
                    continue; // skip current one
                if (poi.isWithinRectangle(Borders{border.top, divisionPoint.y, border.left, divisionPoint.x})) {
                    topLeft.push_back(poi);
                } else if (poi.isWithinRectangle(
                        Borders{border.top, divisionPoint.y, divisionPoint.x, border.right})) {
                    topRight.push_back(poi);
                } else if (poi.isWithinRectangle(
                        Borders{divisionPoint.y, border.bottom, divisionPoint.x, border.right})) {
                    bottomRight.push_back(poi);
                } else if (poi.isWithinRectangle(
                        Borders{divisionPoint.y, border.bottom, border.left, divisionPoint.x})) {
                    bottomLeft.push_back(poi);
                }
            }

            int plotsFound = 0;
            plotsFound += findPlots(topLeft, Borders{border.top, divisionPoint.y, border.left, divisionPoint.x});
            plotsFound += findPlots(topRight, Borders{border.top, divisionPoint.y, divisionPoint.x, border.right});
            plotsFound += findPlots(bottomRight,
                                    Borders{divisionPoint.y, border.bottom, divisionPoint.x, border.right});
            plotsFound += findPlots(bottomLeft, Borders{divisionPoint.y, border.bottom, border.left, divisionPoint.x});

            if (plotsFound > localMaxPlots) {
                localMaxPlots = plotsFound;
                findPlotsResults[id] = plotsFound;
            }
        }

        return localMaxPlots;
    }

    std::string generatePointslistIdentifier(const std::vector<Point> &localPoints) const
    {
        std::string s;
        for (auto point : localPoints) {
            s = s + " " + std::to_string(point.x) + " " + std::to_string(point.y);
        }
        return s;
    }

};


#endif //MAXPLOTS_RECURSIVEAPPROACH_H
