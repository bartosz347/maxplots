#ifndef MAXPLOTS_COMBINATIONPROVIDER_H
#define MAXPLOTS_COMBINATIONPROVIDER_H

#include <string>
#include <algorithm>

class CombinationProvider {
public:
    CombinationProvider(int n, int k) : i(0)
    {
        generateCombinations(n, k);
    }

    bool isFinished() const
    {
        return i >= combinationsVector.size();
    }

    std::vector<int> &getNext()
    {
        return combinationsVector[i++];
    }

private:
    std::vector<std::vector<int>> combinationsVector;
    unsigned int i;

    void generateCombinations(int n, int k)
    {
        std::string bitmask(k, 1);
        bitmask.resize(static_cast<unsigned long long int>(n), 0);

        int j = 0;
        do {
            combinationsVector.emplace_back(std::vector<int>());
            for (int i = 0; i < n; ++i) {
                if (bitmask[i]) {
                    combinationsVector[j].push_back(i);
                }
            }
            j++;
        } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    }
};


#endif //MAXPLOTS_COMBINATIONPROVIDER_H
