#ifndef MAXPLOTS_IMPROVEDBRUTEFORCEMAXPLOT_H
#define MAXPLOTS_IMPROVEDBRUTEFORCEMAXPLOT_H

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "MaxPlotSolver.h"
#include "../structures/Point.h"
#include "CombinationProvider.h"
#include "../structures/Borders.h"

class ImprovedBruteforceMaxPlot : public MaxPlotSolver {
public:
    static const int ALGORITHM_CODE = 2;

    int findMaxPlotsCount(int width, int height, const std::vector<Point> &pointsArg) override
    {
        this->points = pointsArg;
        this->width = width;
        this->height = height;

        if (points.size() == 1) {
            return 1;
        }

        std::sort(points.begin(), points.end(), [](Point a, Point b) {
            return a.x < b.x;
        });

        std::vector<int> combinationSequence;
        std::vector<Point> divisionPoints;
        int maxPlotsFound = 0;

        // for each size of possible subsets of points
        for (unsigned int k = 1; k <= points.size(); k++) {
            CombinationProvider combinationProvider(points.size(), k);
            do {
                // eg. for k=2, sequences are: 1,2 then 1,3 then 1,4 and so on
                combinationSequence = combinationProvider.getNext();

                // mark all points from current combination as division points
                for (unsigned int i = 0; i < k; i++) {
                    points[combinationSequence[i]].isDivision = true;
                    divisionPoints.push_back(points[combinationSequence[i]]);
                }

                int plotsFound = countPlots(divisionPoints);
                if (plotsFound > maxPlotsFound) {
                    maxPlotsFound = plotsFound;
                    divisionPointsForMaxCount = divisionPoints;

                    // k increases with each loop, so first situation when all points either make a plot or are used as division point
                    // means that there's no need to use more points for division - the plots count would decrease
                    if (plotsFound + k == points.size()) {
                        return plotsFound;
                    }
                }

                // remove division points
                for (unsigned int i = 0; i < k; i++) {
                    points[combinationSequence[i]].isDivision = false;
                }
                divisionPoints.clear();

            } while (!combinationProvider.isFinished());
        }

        return maxPlotsFound;
    }

    const std::vector<Point> &getDivisionPoints() const
    {
        return divisionPointsForMaxCount;
    }

private:
    std::vector<Point> points;
    std::vector<Point> divisionPointsForMaxCount;
    int height;
    int width;

    int countPlots(const std::vector<Point> &divisionPoints) const
    {
        int plotsFound = 0;
        for (auto p : points) {
            if (isPlot(p, divisionPoints)) {
                plotsFound++;
            }
        }
        return plotsFound;
    }

    bool isPlot(const Point &p, const std::vector<Point> &divisionPoints) const
    {
        if (p.isDivision) {
            return false;
        }

        Borders border = findBorders(p, divisionPoints);

        // check if any others points exist within borders
        for (auto poi : points) {
            if (poi.isDivision) continue;
            if (poi.isWithinRectangle(border) && p != poi) {
                return false;
            }
            if (poi.x > border.right) {
                break; // all points outside the borders can be skipped
            }
        }

        return true;
    }

    Borders findBorders(const Point &p, const std::vector<Point> &divisionPoints) const
    {
        Borders pointsBorder(height, 0, 0, width);
        for (auto divisionPoint: divisionPoints) {
            if (divisionPoint.y > p.y && divisionPoint.y < pointsBorder.top) {
                pointsBorder.top = divisionPoint.y;
            }

            if (divisionPoint.y < p.y && divisionPoint.y > pointsBorder.bottom) {
                pointsBorder.bottom = divisionPoint.y;
            }

            if (divisionPoint.x < p.x && divisionPoint.x > pointsBorder.left) {
                pointsBorder.left = divisionPoint.x;
            }

            if (divisionPoint.x > p.x && divisionPoint.x < pointsBorder.right) {
                pointsBorder.right = divisionPoint.x;
            }
        }

        return pointsBorder;
    }
};


#endif //MAXPLOTS_IMPROVEDBRUTEFORCEMAXPLOT_H
