#ifndef MAXPLOTS_MAXPLOTSOLVER_H
#define MAXPLOTS_MAXPLOTSOLVER_H

#include "../structures/Point.h"

class MaxPlotSolver {
public:
    virtual int findMaxPlotsCount(int width, int height, const std::vector<Point> &points) = 0;
    virtual const std::vector<Point> &getDivisionPoints() const = 0;
};


#endif //MAXPLOTS_MAXPLOTSOLVER_H
