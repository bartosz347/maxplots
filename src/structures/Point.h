#ifndef MAXPLOTS_POINT_H
#define MAXPLOTS_POINT_H


#include "Borders.h"

class Point {
public:
    Point(int x, int y) : x(x), y(y)
    {}

    Point()
    {}

    int x;
    int y;
    bool isDivision = false;

    bool isWithinRectangle(const Borders &border)
    {
        return (x > border.left && x < border.right)
               && (y > border.bottom && y < border.top);
    }
};

inline bool operator!=(const Point &lhs, const Point &rhs)
{
    return lhs.x != rhs.x || lhs.y != rhs.y;
}

inline bool operator==(const Point &lhs, const Point &rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

#endif //MAXPLOTS_POINT_H
