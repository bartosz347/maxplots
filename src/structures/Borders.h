#ifndef MAXPLOTS_BORDERS_H
#define MAXPLOTS_BORDERS_H

#include <stdexcept>

class Borders {
public:
    int top, bottom, left, right;

    Borders() : top(-1), bottom(-1), left(-1), right(-1) {}

    Borders(int top, int bottom, int left, int right) : top(top), bottom(bottom), left(left), right(right)
    {}

    void validate()
    {
        if (top == -1 || bottom == -1 || left == -1 || right == -1) {
            throw std::runtime_error("border not found");
        }
    }


};

inline bool operator==(const Borders &lhs, const Borders &rhs)
{
    return lhs.top == rhs.top && lhs.bottom == rhs.bottom
           && lhs.left == rhs.left && lhs.right == rhs.right;
}

#endif //MAXPLOTS_BORDERS_H
