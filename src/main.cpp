#include "cassert"
#include <iostream>
#include <memory>
#include <cstring>
#include <chrono>
#include <iomanip>

#include "algorithms/BoardGenerator.h"
#include "algorithms/ImprovedBruteforceMaxPlot.h"
#include "analysis/ComplexityAnalyser.h"

void printUsage(char *const *argv);

void runSolver(int width, int height, const std::vector<Point> &points, const int i);

void runTests(int initialCount, int problemsCount, int stepSize, int testsPerInstance, int algorithmCode);

void checkDynamicProgrammingSolutionQuality(int initialCount, int problemsCount, int stepSize, int testsPerInstance);

int main(int argc, char *argv[])
{
    int width, height, pointsCount;
    std::vector<Point> points;
    try {
        if (argc != 2 && argc != 5 && argc != 7 && argc != 6) {
            printUsage(argv);
        } else if (strcmp(argv[1], "-i") == 0) {
            // solve from stdin
            std::cin >> width >> height >> pointsCount;
            for (int i = 0; i < pointsCount; ++i) {
                Point p;
                std::cin >> p.x >> p.y;
                points.push_back(p);
            }
            if (pointsCount > std::min(height - 1, width - 1)) {
                throw std::runtime_error("too many points for board");
            }
            runSolver(width, height, points, ImprovedBruteforceMaxPlot::ALGORITHM_CODE);
        } else if (strcmp(argv[1], "-g") == 0 && argc == 5) {
            // generate and solve
            width = atoi(argv[2]);
            height = atoi(argv[3]);
            pointsCount = atoi(argv[4]);
            BoardGenerator s;
            points = s.generatePoints(width, height, pointsCount);
            runSolver(width, height, points, ImprovedBruteforceMaxPlot::ALGORITHM_CODE);
        } else if (strcmp(argv[1], "-t") == 0 && argc == 7) {
            //run tests <method> <initial points count> <problems to solve> <step size> <tests per instance>
            int initialCount = atoi(argv[3]);
            int problemsCount = atoi(argv[4]);
            if (problemsCount % 2 == 0) {
                throw std::runtime_error("problems count must be an odd number");
            }
            int stepSize = atoi(argv[5]);
            int testsPerInstance = atoi(argv[6]);
            std::string method(argv[2]);
            if (method == "bf") {
                runTests(initialCount, problemsCount, stepSize, testsPerInstance,
                         BruteforceMaxPlot::ALGORITHM_CODE);
            } else if (method == "dyn") {
                runTests(initialCount, problemsCount, stepSize, testsPerInstance, RecursiveApproach::ALGORITHM_CODE);
            } else {
                printUsage(argv);
            }
        } else if (strcmp(argv[1], "-q") == 0 && argc == 6) {
            //quality tests dp vs brute-force <initial points count> <problems to solve> <step size> <tests per instance>
            int initialCount = atoi(argv[2]);
            int problemsCount = atoi(argv[3]);
            int stepSize = atoi(argv[4]);
            int testsPerInstance = atoi(argv[5]);
            checkDynamicProgrammingSolutionQuality(initialCount, problemsCount, stepSize, testsPerInstance);
        } else {
            printUsage(argv);
        }
    } catch (std::runtime_error e) {
        std::cout << "error: " << e.what();
    }

    return 0;
}

void runTests(int initialCount, int problemsCount, int stepSize, int testsPerInstance, int algorithmCode)
{
    using namespace std;
    using namespace std::chrono;

    std::unique_ptr<MaxPlotSolver> maxPlotSolver;
    std::string asymptote;
    switch (algorithmCode) {
        case BruteforceMaxPlot::ALGORITHM_CODE:
            maxPlotSolver = std::make_unique<BruteforceMaxPlot>();
            asymptote = "O(2^n * n)";
            break;
        case RecursiveApproach::ALGORITHM_CODE:
            maxPlotSolver = std::make_unique<RecursiveApproach>();
            asymptote = "O(n^4)";
            break;
        default:
            throw std::runtime_error("unknown algorithm type");
    }
    BoardGenerator boardGenerator{};
    ComplexityAnalyser complexityAnalyser(algorithmCode);
    int pointsCount = initialCount;
    for (int i = 0; i < problemsCount; i++) {
        long long int duration = 0;
        for (int j = 0; j < testsPerInstance; j++) {
            int width = pointsCount + 1;
            int height = pointsCount + 1;
            auto points = boardGenerator.generatePoints(width, height, pointsCount);
            high_resolution_clock::time_point startTime = high_resolution_clock::now();
            maxPlotSolver->findMaxPlotsCount(width, height, points);
            high_resolution_clock::time_point endTime = high_resolution_clock::now();
            duration += duration_cast<milliseconds>(endTime - startTime).count();
        }
        duration = duration / testsPerInstance;
        complexityAnalyser.addResult(pointsCount, duration);
        pointsCount += stepSize;
    }

    std::cout << "Algorytm z asymptota " <<  asymptote << std::endl;
    std::cout << "n\tt(n) [ms]\tq(n)" << std::endl;
    auto results = complexityAnalyser.getResults();
    for (auto result : results) {
        std::cout << result.n << "\t"
                  << result.duration << "\t\t"
                  << std::setprecision(3) << result.conformityCoefficient << std::endl;
    }
}

void runSolver(int width, int height, const std::vector<Point> &points, const int algorithmCode)
{
    std::unique_ptr<MaxPlotSolver> maxPlotSolver;
    switch (algorithmCode) {
        case ImprovedBruteforceMaxPlot::ALGORITHM_CODE:
            maxPlotSolver = std::make_unique<ImprovedBruteforceMaxPlot>();
            break;
        case RecursiveApproach::ALGORITHM_CODE:
            maxPlotSolver = std::make_unique<RecursiveApproach>();
            break;
        default:
            throw std::runtime_error("unknown algorithm type");
    }

    std::cout << "result = " << maxPlotSolver->findMaxPlotsCount(width, height, points) << std::endl;
    if (algorithmCode != RecursiveApproach::ALGORITHM_CODE) {
        std::cout << "division points:" << std::endl;
        auto divisionPoints = maxPlotSolver->getDivisionPoints();
        for (auto point : divisionPoints) {
            std::cout << point.x << " " << point.y << std::endl;
        }
    }
}

void checkDynamicProgrammingSolutionQuality(int initialCount, int problemsCount, int stepSize, int testsPerInstance)
{
    ImprovedBruteforceMaxPlot improvedBruteforceMaxPlot;
    RecursiveApproach recursiveApproach;
    BoardGenerator boardGenerator;
    int pointsCount = initialCount;
    std::cout << "n\toptimal\twrong\tsuccess rate" << std::endl;
    for (int i = 0; i < problemsCount; i++) {
        int optimal = 0, wrong = 0;
        for (int j = 0; j < testsPerInstance; j++) {
            int width = pointsCount + 1;
            int height = pointsCount + 1;
            auto points = boardGenerator.generatePoints(width, height, pointsCount);
            int referenceResult = improvedBruteforceMaxPlot.findMaxPlotsCount(width, height, points);
            int result = recursiveApproach.findMaxPlotsCount(width, height, points);
            if (referenceResult == result) {
                optimal++;
            } else {
                wrong++;
            }
        }
        double successRate = static_cast<double>(optimal * 100) / testsPerInstance;
        std::cout << pointsCount << "\t"
                  << optimal << "\t"
                  << wrong << "\t"
                  << std::setprecision(3) << successRate << "%"
                  << std::endl;
        pointsCount += stepSize;
    }
}

void printUsage(char *const *argv)
{
    std::cout << "Usage: " << argv[0] << " OPTION" << std::endl
              << "\t-i   to read data from standard input" << std::endl
              << "\t     data format:" << std::endl << "\t\twidth height pointsCount" << std::endl
              << "\t\tx y" << std::endl << "\t\tx y" << std::endl << "\t\t..." << std::endl;
    std::cout << "\t-g <width> <height> <pointsCount>   to generate data" << std::endl;
    std::cout
            << "\t-t <method: bf | dyn> <initial points count> <problems to solve> <step size> <tests per instance>   to run complexity tests" << std::endl
            << "\t-q <initial points count> <problems to solve> <step size> <tests per instance>   to run quality tests"
            << std::endl;
}

