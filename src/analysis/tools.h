#ifndef MAXPLOTS_TOOLS_H
#define MAXPLOTS_TOOLS_H

void runBasicTests(MaxPlotSolver &maxPlotSolver);

void findPossibleResults(int width, int height, int pointsCount);

#include <cassert>
#include "../algorithms/BoardGenerator.h"
#include "../algorithms/ImprovedBruteforceMaxPlot.h"
#include "../algorithms/RecursiveApproach.h"

void findPossibleResults(int width, int height, int pointsCount)
{
    BoardGenerator s;
    ImprovedBruteforceMaxPlot bruteforceMaxPlot;
    std::vector<int> possibleResults;
    for (int i = 0; i < 90000; i++) {
        auto data = s.generatePoints(width, height, pointsCount);
        int res = bruteforceMaxPlot.findMaxPlotsCount(width, height, data);
        if (std::find(possibleResults.begin(), possibleResults.end(), res) == possibleResults.end()) {
            possibleResults.push_back(res);
            std::cout << res << std::endl;
        }
    }
}

void runBasicTests(MaxPlotSolver &maxPlotSolver)
{
    assert(maxPlotSolver.findMaxPlotsCount(4, 3, {{2, 2}}) == 1);
    assert(maxPlotSolver.findMaxPlotsCount(7, 4, {{1, 3},
                                                  {3, 2},
                                                  {5, 1}}) == 2);
    assert(maxPlotSolver.findMaxPlotsCount(4, 3, {{1, 1},
                                                  {2, 2}}) == 1);
    assert(maxPlotSolver.findMaxPlotsCount(4, 4, {{3, 1},
                                                  {2, 2}}) == 1);
    assert(maxPlotSolver.findMaxPlotsCount(10, 7, {{2, 2},
                                                   {5, 3},
                                                   {7, 1},
                                                   {8, 4},
                                                   {4, 6}}) == 4);
    assert(maxPlotSolver.findMaxPlotsCount(6, 6, {{3, 1},
                                                  {1, 5},
                                                  {2, 4},
                                                  {5, 3},
                                                  {4, 3},
                                                  {5, 2}}) == 3);
    assert(maxPlotSolver.findMaxPlotsCount(6, 6, {{1, 4},
                                                  {2, 2},
                                                  {4, 1},
                                                  {5, 5},
                                                  {3, 3}}) == 4);
    assert(maxPlotSolver.findMaxPlotsCount(6, 6, {{1, 1},
                                                  {2, 2},
                                                  {3, 5},
                                                  {4, 4},
                                                  {5, 3}}) == 3);
    assert(maxPlotSolver.findMaxPlotsCount(9, 9,
                                           {{1, 5},
                                            {3, 2},
                                            {5, 7},
                                            {2, 6},
                                            {4, 1},
                                            {7, 3},
                                            {8, 4},
                                            {6, 8}}) == 4);
    // recursive solution returns wrong result
    assert(maxPlotSolver.findMaxPlotsCount(7, 7, {{1, 2},
                                                  {2, 4},
                                                  {3, 6},
                                                  {5, 3},
                                                  {6, 5},
                                                  {4, 1}}) == 4);
    // recursive solution returns wrong result
    assert(maxPlotSolver.findMaxPlotsCount(11, 11,
                                           {{1,  10},
                                            {2,  5},
                                            {3,  1},
                                            {4,  8},
                                            {5,  2},
                                            {6,  7},
                                            {7,  4},
                                            {8,  9},
                                            {9,  6},
                                            {10, 3}}) == 8);
}

#endif //MAXPLOTS_TOOLS_H
