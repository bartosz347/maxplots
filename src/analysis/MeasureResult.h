#ifndef MAXPLOTS_MEASURERESULT_H
#define MAXPLOTS_MEASURERESULT_H


class MeasureResult {
public:
    int n;
    long long int duration;
    double conformityCoefficient;

    MeasureResult(int n, long long int duration) : n(n), duration(duration)
    {}
};


#endif //MAXPLOTS_MEASURERESULT_H
