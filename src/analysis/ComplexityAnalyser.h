#ifndef MAXPLOTS_CONFORMITYCOEFFICIENTSCALCULATOR_H
#define MAXPLOTS_CONFORMITYCOEFFICIENTSCALCULATOR_H


#include <vector>
#include "MeasureResult.h"
#include "../algorithms/RecursiveApproach.h"
#include "../algorithms/BruteforceMaxPlot.h"

class ComplexityAnalyser {

public:
    ComplexityAnalyser(int algorithm) : algorithm(algorithm) {}

    void addResult(int n, long long int duration)
    {
        measureResults.push_back(MeasureResult(n, duration));
    }

    std::vector<MeasureResult> &getResults()
    {
        calculateConformityCoefficients();
        return measureResults;
    }

private:
    const int algorithm;
    std::vector<MeasureResult> measureResults;

    long long int getTOfn(int n)
    {
        if (algorithm == BruteforceMaxPlot::ALGORITHM_CODE) {
            // 2^n * n
            return (1 << n) * n;
        } else if (algorithm == RecursiveApproach::ALGORITHM_CODE) {
            // n ^ 4
            return n * n * n * n;
        } else {
            throw std::runtime_error("unknown algorithm");
        }
    }

    void calculateConformityCoefficients()
    {
        int nMedian = measureResults[measureResults.size() / 2].n;
        long long int tOfnMedian = measureResults[measureResults.size() / 2].duration;
        long long int TOfnMedian = getTOfn(nMedian);

        for (unsigned int i = 0; i < measureResults.size(); i++) {
            double q = (static_cast<double>(measureResults[i].duration * TOfnMedian))
                       / (getTOfn(measureResults[i].n) * tOfnMedian);
            measureResults[i].conformityCoefficient = q;
        }
    }
};


#endif //MAXPLOTS_CONFORMITYCOEFFICIENTSCALCULATOR_H
